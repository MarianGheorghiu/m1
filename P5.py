def square(x):
    return (x ** 2)


def func_5(aFunc, aSeq):
    result = []
    for x in aSeq:
        result.append(aFunc(x))
    return result


print(func_5(square, [1, 1, 2, 3, 5, 8]))
