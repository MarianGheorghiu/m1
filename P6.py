def func_6(items):
    if type(items) == int:
        return lambda x, y: -1 if x < y else 0 if x == y else 1
    return lambda x, y: -1 if len(x) < len(y) else 0 if len(x) == len(y) else 1


cmp1 = func_6(0)
cmp2 = func_6('')

print(cmp1(2, 3))

print(cmp2('ana are', 'adi are'))
