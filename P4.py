def func(file1, file2, file3):

    for item in file1:
        with open('countries.txt', 'r') as file1:
            print(file1.read())

    for item2 in file2:
        with open('visited.txt', 'r') as file2:
            print(file2.read())

    for item3 in file3:
        with open('countries.txt', 'r') as file1:
            with open('visited.txt', 'r') as file2:
                difference = set(file1).difference(file2)

        with open('to_be_visited.txt', 'w+') as file_out:
            for line in difference:
                file_out.write(line)


print(func('file1', 'file2', 'file3'))
