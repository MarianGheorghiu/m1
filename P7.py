import pickle
import os
from collections import defaultdict


def getmenuchoice():
    print('1\tShow agenda')
    print('2\tAdd contact')
    print('3\tAdd phone nr to contact')
    print('4\tFind contact')
    print('5\tDelete contact')
    print('0\tExit')


directory = defaultdict(list)


ch = getmenuchoice()


ch = True
while ch:
    ch = input('Select option: ')
    print()
    if ch == '1':
        print(directory)

    elif ch == '2':
        name = input('Name: ')
        number = input('Number: ')
        directory[name].append(number)

    elif ch == '3':
        name = input('Add number to: ')
        number2 = input('Number2: ')
        directory[name].append(number2)
        print('Second number has successfully added.')

    elif ch == '4':
        name = input('Search: ')
        if name in directory:
            print(name, 'is in list with number: ', directory[name])

        else:
            print('This person is not in the list.')

    elif ch == '5':
        remove = input('Who you want to delete ?: ')
        if remove in directory:
            del directory[remove]
            print('Contact deleted.')
    elif ch == '0':
        break
