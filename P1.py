def func_1(*args):
    maximum = max(*args)
    minimum = min(*args)
    return (minimum, maximum)


print(func_1(73, 21, 7, 321, 5, 8))
